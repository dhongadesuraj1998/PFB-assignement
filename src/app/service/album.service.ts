import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  constructor(private http: HttpClient) { }

  getAlbumData(from:any,to:any,search:any): Observable<any> {
    return this.http.get(`https://jsonplaceholder.typicode.com/albums?search=${search}from=${from}&to=${to}`).pipe(
      map(res => res)
    )
  }

  getAlbumById(id:any): Observable<any> {
    return this.http.get(`https://jsonplaceholder.typicode.com/albums?id=${id}`).pipe(
      map(res => res)
    )
  }

  getUserDAta(id: any): Observable<any> {
    return this.http.get(`https://jsonplaceholder.typicode.com/users?id=${id}`).pipe(
      map(res => res)
    )
  }

  getPhototsById(id: any): Observable<any> {
    return this.http.get(`https://jsonplaceholder.typicode.com/photos?albumId=${id}`).pipe(
      map(res => res)
    )
  }
}
