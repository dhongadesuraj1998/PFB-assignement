import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: '', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
    },

];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        initialNavigation: 'enabled'
    }
    )],
    exports: [RouterModule]
})
export class AppRoutingModule { }
