import { Pipe, PipeTransform } from '@angular/core';
import { Album } from './album'

@Pipe({
    name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

    transform(Album: Album[], searchValue: string): Album[] {

        if (!Album || !searchValue) {
            return Album
        }
        return Album.filter(album =>
            album.title.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()) ||
            album.id.toString().includes(searchValue.toLocaleLowerCase()) ||
            album.userId.toString().includes(searchValue.toLocaleLowerCase())
        )
    }

}
