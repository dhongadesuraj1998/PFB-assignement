import { Component, OnInit } from '@angular/core';
import { AlbumService } from '../../service/album.service';
import { Router } from '@angular/router';
import { Album } from '../../album'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  Album!: Album[];
  
  id: any;
  albumId: any;
  singleAlbum: any;
  user: any
  count: any;
  photoList: any;
  errorMessage:any;
  searchValue!: any;

  paginationObj = {
    page: 1,
    size: 10
  };

  constructor(
    private albumService: AlbumService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getAlbumData();
  }

  getAlbumData() {
    let from = 1
    let to = 10
    this.albumService.getAlbumData(from,to,'').subscribe((res: any) => {
      this.Album = res;
      this.count = this.Album.length;
    },(err)=>{
      if(err.error){
       this.errorMessage = "Server error...."
      }
    })
  }

  getAlbumDataById(album: any) {
    this.albumService.getAlbumById(album.id).subscribe((res: any) => {
      this.singleAlbum = res;
    },(err)=>{
      if(err.error){
        this.errorMessage = "Server error...."
       }
    })
    this.id = album.userId;
    this.getUserData();
    this.albumId = album.id
    this.getPhotos(this.albumId)
  }

  getUserData() {
    this.albumService.getUserDAta(this.id).subscribe((res: any) => {
      this.user = res
    },(err)=>{
      if(err.error){
        this.errorMessage = "Server error...."
       }
    })
  }

  getPhotos(id: any) {
    this.albumService.getPhototsById(id).subscribe((res: any) => {
      this.photoList = res;
    },(err)=>{
      if(err.error){
        this.errorMessage = "Server error...."
       }
    })
  }

  pageChanged($event: any) {
    this.paginationObj.page = $event;
    let from,to
    if($event==1){
       from = 1
       to = 10
    }else{
        from=($event*10)-9
        to=($event*10)
    }
    this.albumService.getAlbumData(from,to,'').subscribe((res: any) => {
      this.Album = res;
      this.count = this.Album.length;
    },(err)=>{
      if(err.error){
        this.errorMessage = "Server error...."
       }
    })
  }

}
