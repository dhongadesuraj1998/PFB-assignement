import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {NgxPaginationModule} from 'ngx-pagination'; 

import { AlbumService } from '../service/album.service'

import { DashboardRoutingModule } from './dashboard-routing.module';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ProfileComponent } from './profile/profile.component';

import { SearchFilterPipe } from '../search-filter.pipe';

@NgModule({
  declarations: [
    HomeComponent, 
    SidebarComponent, 
    ProfileComponent,
    SearchFilterPipe
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  exports:[SidebarComponent],
  providers:[AlbumService]
})
export class DashboardModule { }
